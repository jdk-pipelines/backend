package com.jdkexample.demopipelines.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.HashMap;
import java.util.Map;

@RestController()
@RequestMapping("/echo")
@RequiredArgsConstructor(onConstructor_ = {@Autowired})
public class EchoController {


    @Value("${server.version}")
    private String version;

    @Autowired
    private Environment environment;

    @GetMapping("")
    public Map<String, Object> get() {
        Map<String, Object> out = new HashMap<>();
        out.put("name", "JDK Pipelines");
        out.put("version", version);
        out.put("activeProfiles", environment.getActiveProfiles());
        return out;
    }
}

package com.jdkexample.demopipelines;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//@Profile({"dev", "test", "prod"})
public class DemoPipelinesApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoPipelinesApplication.class, args);
	}

}
